% Progression cybersécurité des applications
% par [moulinux](https://moulinux.frama.io/)
% (révision 12.09.2021)

Cette progression s'inscrit dans le parcours pour les développeurs du BTS
SIO (option SLAM) :

* Elle commence par une [introduction] à la cybersécurité des applications;
* Elle se poursuit par un retour sur les [failles XSS][xss], *Cross Site Scripting* et XSS réfléchi.


[introduction]: https://cybersec.frama.io/0-introduction
[xss]: https://framagit.org/CyberSec/1-XSS-reflechi
